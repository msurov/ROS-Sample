Installation:
============
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 0xB01FA116
sudo apt-get install ros-jade-ros-base

sudo rosdep init
sudo rosdep update

for more info:
-------------

http://wiki.ros.org/jade/Installation
http://wiki.ros.org/kinetic/Installation


Running the packages:
====================

begin working:
-------------

	```bash
	cd ~/Documents/dev/ros/ROS-Sample
	source init.sh
	gnome-terminal -e "roscore"
	gnome-terminal -e "rosrun simple_client simple_client"
	gnome-terminal -e "rosrun incrementor incrementor"
	```

create new package:
------------------

	```bash
	roscreate-pkg [new package name] std_msgs roscpp ...
	```

run existing package:
--------------------

	```bash
	rosrun [package name] [routine name]
	```

find package:
------------

	```bash
	rospack find tutorial
	```

show dependencies:
-----------------

	```bash
	rospack depends tutoria
	```

add message types to pubisher
-----------------------------
	create the file:

	```bash
	echo "string str" > msg/Str.msg
	```


Example:
=======
	create packet "tutorial" with dependencies: std_msgs rospy roscpp
	```
	roscreate-pkg tutorial std_msgs rospy roscpp
	echo "string str" > msg/Str.msg
	rospack profile
	rospack find tutorial
	rospack depends tutoria
	```

Share messages between computers:
================================
server:
	export ROS_MASTER_URI=http://maksim-nuc:11311
client:	
	export ROS_MASTER_URI=http://192.168.0.103:11311


