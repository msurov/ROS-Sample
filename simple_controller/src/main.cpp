#include <string>
#include <iostream>
#include <vector>
#include <mutex>
#include <fstream>
#include <memory>
#include <time.h>
#include "ros/ros.h"
#include "std_msgs/String.h"

using namespace std;


const string node_name = "simple_controller";
const string msg_torque = "torque";
const string msg_state = "state";
const int queue_torque_len = 10;
const int queue_state_len = 10;


class controller
{
private:
    ros::Subscriber sub;
    ros::Publisher pub;
    unique_ptr<ros::NodeHandle> node;

public:
    void requests_handler(const std_msgs::String::ConstPtr& input)
    {
        std_msgs::String output;
        output.data = input->data;
        pub.publish(output);
    }

    controller(int argc, char **argv)
    {
        ros::init(argc, argv, node_name);
        node.reset(new ros::NodeHandle());
        sub = node->subscribe(msg_state, queue_torque_len, &controller::requests_handler, this);
        pub = node->advertise<std_msgs::String>(msg_torque, queue_state_len);
    }

    ~controller()
    {
    }
};

int main(int argc, char **argv)
{
	unique_ptr<controller> ctrl;
	ctrl.reset(new controller(argc, argv));
    ros::spin();
    return 0;
}
