#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <memory>
#include <time.h>
#include <unistd.h>
#include <signal.h>

#include "ros/ros.h"
#include "std_msgs/String.h"


using namespace std;


const string node_name = "simple_embedded";
const string msg_torque = "torque";
const string msg_state = "state";
const int queue_torque_len = 10;
const int queue_state_len = 10;

struct cyclic_list;
shared_ptr<cyclic_list> dump;


struct cyclic_list
{
    vector<string> data;
    size_t pointer;

    cyclic_list(int size) : data(size), pointer(0) 
    {
    }

    ~cyclic_list()
    {
    }

    void append(string const& msg)
    {
        data[pointer] = msg;
        pointer = (pointer + 1) % data.size();
    }
};

void dump_list(ostream& s, cyclic_list const& list)
{
    for (size_t i = list.pointer; i < list.data.size(); ++ i)
        s << list.data[i] << endl;

    for (size_t i = 0; i < list.pointer; ++ i)
        s << list.data[i] << endl;
}

int64_t get_usec()
{
    struct timeval tv;
    gettimeofday(&tv,NULL);
    return tv.tv_sec*(int64_t)1000000+tv.tv_usec;
}

void requests_handler(const std_msgs::String::ConstPtr& msg)
{
    int64_t t1 = get_usec();
    int64_t t0 = stoll(msg->data);
    int64_t dt = t1 - t0;
    string data = "msg ts " + to_string(t1) + ", " + to_string(t0) + ", " + to_string(dt);
    dump->append(data);
}

int main(int argc, char **argv)
{
    dump = make_shared<cyclic_list>(1000);
    // signal(SIGINT, &break_handler);

    ros::init(argc, argv, node_name);
    ros::NodeHandle n;

    ros::Subscriber sub = n.subscribe(msg_torque, queue_torque_len, requests_handler);
    ros::Publisher pub = n.advertise<std_msgs::String>(msg_state, queue_state_len);

    ros::Rate loop_rate(100);
    ros::AsyncSpinner spinner(2);
    spinner.start();

    while (ros::ok())
    {
        std_msgs::String msg;
        msg.data = to_string(get_usec());
        pub.publish(msg);
        loop_rate.sleep();
    }


    spinner.stop();

    cout << "[info] saving logfile" << endl;

    ofstream f("log.txt");
    dump_list(f, *dump);
    f.close();

    return 0;
}
