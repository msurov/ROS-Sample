#!/bin/bash
set -e

cd simple_controller
make
cd ..
cd simple_embedded
make
cd ..

# export ROS_IP=192.168.7.1
# export ROS_MASTER_URI=http://beaglebone-nuc:11311

gnome-terminal -e "roscore"
gnome-terminal -e "rosrun simple_controller simple_controller"
gnome-terminal -e "rosrun simple_embedded simple_embedded"
