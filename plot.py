import re
from matplotlib import pyplot as plt

pattern = r'msg ts (\d+), (\d+), (\d+)'
data = []

for line in open('log.txt', 'r'):
	if len(line) == 0:
		continue

	ans = re.match(pattern, line)
	if ans is not None:
		data.append(int(ans.group(3)))


plt.plot(data)
plt.grid()
plt.show()

# msg ts 1466296747502718, 1466296747502425, 293
